// Теоретичні питання
// 1. Опишіть своїми словами як працює метод forEach.

// forEach викликає функцію для всіх елементів масивую На приклад:
// const fruits = ["apple", "orange", "cherry"];
// fruits.forEach(myFunction);

// 2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

// мутуючі методи змінюють існуючий масив. На приклад: push() , pop(), unshift(), shift(), splice()
let flavours = ["appel", "orange", "banana"];
flavours.push("melon", "strawberry");
let newLength = flavours.push("melon", "strawbwrry");
console.log("array",flavours);
console.log("new length", newLength);

// немутуючі методи не змінюють існуючий масив, а повертають новий. 
// На приклад: slice(), concat(), map(), filter(), reduce(), sort(), reverse(), every(), some()
let nums = [1, 2, 3, 4, 5];
console.log(nums.slice(1, 3));

console.log(nums.filter(function(element) {
    return element % 2 === 0;
})
);

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Можна за допомогою методу length()
const myArray = ["Horses", "Dogs", "Cats"];
console.log("Array length", myArray.length);

const myEmptyArray = [];
console.log("Array length", myEmptyArray.length);

// 4. В яких випадках краще використовувати метод map(), а в яких forEach()?
// Метод forEach(), фактично, нічого не повертає (undefined). 
// Він просто викликає надану функцію для кожного елемента у масиві. 
// Цей зворотний виклик дозволяє змінювати викликаний масив.

// Метод map() теж викличе надану функцію для кожного елемента масиву. 
// Відмінність полягає в тому, що map() використовує значення, які повертаються, 
// і повертає новий масив того самого розміру.

let arr = [1, 2, 3, 4, 5];
arr.forEach(num => {
    console.log("forEach result:", num * 2);
});
let arr2 = arr.map(num => num * 2).filter(num => num > 5);
console.log(arr2);// arr2 = [6, 8, 10]




// Практичні завдання
// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків 
// з довжиною більше за 3 символи. Вивести це число в консоль.
let randomWords = ["travel", "hello", "eat", "ski", "lift"];

let totalStrings = randomWords.filter(str => str.length > 3);
console.log("filtered string:", totalStrings);
console.log("filtered string length:", totalStrings.length);


// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. 
// Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
// Відфільтрований масив виведіть в консоль.

const humans = [
    {name: "Ivanko", 
    age: 25, 
    sex: "male"},

    {name: "Stepanko", 
    age: 18.5, 
    sex: "hermaphrodite"},

    {name: "Marusia", 
    age: 65, 
    sex: "female"},

    {name: "Darusia", 
    age: 41, 
    sex: "female"},

    {name: "Petrus'", 
    age: 35, 
    sex: "male"},
];

let humansMale = humans.filter(human => human.sex === "male");
console.log("Male human:", humansMale);



// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

// Технічні вимоги:


// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, 
// який міститиме будь-які дані, другий аргумент - тип даних.

// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
// за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив
// ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

function filterBy(arr, type) {
   return arr.filter(value => typeof(value)!== type)
}
 console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
